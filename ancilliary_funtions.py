#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
File: ancilliary_functions.py
Description:
"""

import pandas as pd # version 0.25.1
import numpy as np # version 1.17.2
from scipy import stats # version 1.4
import matplotlib.pyplot as plt # version 3.1.1
import seaborn as sns # version 0.9.0
from sklearn.metrics import classification_report,confusion_matrix # version 0.9.0 


# Análisis inicial de variables categóricas y otras.
# Entrada: datamart
def analizar_variables(datamart):
    dm_type = datamart.dtypes
    
    # Mostrar .describe() de las variables numéricas
    print('Variables numéricas')
    display(datamart[dm_type[dm_type != 'object'].index].describe())
    
    # Obtener lista de columnas con object
    lista_object = dm_type[dm_type == 'object'].index
    
    if len(lista_object) > 0:
        print('\n' + '-'*80)
        print('\nVariables no numéricas')
        # Mostrar .describe() de las variables object
        display(datamart[lista_object].describe())

        # Mostrar .value_counts() de las variables object
        for i in lista_object:
            display(pd.DataFrame({ i: datamart[i].value_counts(), 
                                   '%': datamart[i].value_counts('%')}))
    
    print('\n' + '-'*80)
    print('\nCantidad de valores nulos')
    display(datamart.isnull().sum())


## Analisis visual
# Muestra en n columnas
# usar con plt.figure(figsize=(8,20))
# diferencia entre categoricas (objet), numericas y binarias.
# Las categorias solo muestra las primeras 15 en orden.
# Entrada: datamart
# Opcional: 
#   col: cantidad de columnas
#   categorias: categorias a graficar.
#   sizefact: 
def inspeccion_visual(datamart, col=3, categorias=15, sizefact=5):
    df_temp = datamart.copy()
    row = np.ceil(df_temp.columns.size / col)
    
    dm_type = df_temp.dtypes
    index = 1
    
    plt.figure(figsize=(sizefact*col, sizefact*row))

    for columna, tipo in df_temp.dtypes.iteritems():
        plt.subplot(row, col, index)
        
        if tipo == 'object':
            df_temp[columna].value_counts().iloc[:categorias].index
            g = sns.countplot(df_temp[columna], 
                              order=df_temp[columna].value_counts().iloc[:categorias].index)
            plt.xticks(rotation=90)
            plt.axhline(df_temp[columna].value_counts().mean(), color='forestgreen', linestyle='--')
        else:
            if len(df_temp[columna].value_counts()) > 2:
                sns.distplot(df_temp[columna], hist=True, kde=True, fit=stats.norm )
                plt.axvline(df_temp[columna].mean(), color='forestgreen', linestyle='--')
            else:
                sns.countplot(df_temp[columna])
                plt.axhline(df_temp[columna].value_counts().mean(), color='forestgreen', linestyle='--')
        
        index += 1

    plt.tight_layout()
    plt.show();
 
 
# Histograma con media y mediana
# Entrada: dataframe, vaiable a graficar
# opcional: cantidad de bin, si grafica la curva KDE, si grafica la Normal.
# si lo transforma a logaritmo, usa el valor 'eta' para los log de cero
def plot_hist(dataframe, variable, bins='auto', kde=False, norm=False, log=False):
    tmp = dataframe.copy()

    if log:
        serie = np.log1p(tmp[variable])
        #serie[np.isneginf(serie)] = 0
        texto = 'logaritmo de '
    else:
        serie = tmp[variable]
        texto = ''
    
    if norm:
        fit = stats.norm
    else:
        fit = None
        
    media = serie.mean()
    mediana = serie.median()
    
    sns.distplot(serie, hist=True, bins=bins, color='blue', axlabel=False,
             hist_kws={'edgecolor':'darkblue'},
             kde=kde, kde_kws={'linewidth': 4, 'label': 'KDE'},
             fit=fit, fit_kws={'label': 'Norm'}
             )
    
    label = 'Media: {:.1f}'.format(media)
    plt.axvline(media, color='green', label=label, linewidth=2)
    
    label = 'Mediana: {:.1f}'.format(mediana)
    plt.axvline(mediana, color='tomato', label=label, linewidth=2)
    
    plt.title('Histograma para ' + texto + variable, weight='bold').set_fontsize('10')
    plt.legend();


# Muestra la matriz de confusion
# y_test: vector objetivo de prueba
# pred_y: vector objetivo de la predicción
def mostrar_resultados(y_test, pred_y):
    conf_matrix = confusion_matrix(y_test, pred_y)
    plt.figure(figsize=(6, 6))
    sns.heatmap(conf_matrix, annot=True, fmt="d", cbar=False);
    plt.title("Confusion matrix")
    plt.ylabel('True class')
    plt.xlabel('Predicted class')
    plt.show()
    print (classification_report(y_test, pred_y))


# Graficación del Recall en los modelos
# Comparacion de valor de métrica entre modelos
# Entrada: Modelos a comparar (recall_decisiontree,recall_randomforest,recall_gradientboost,recall_xgbost)
def plot_recall_modelos(recall_decisiontree,recall_randomforest,recall_gradientboost,recall_xgbost):
    labels = ['Clase 0', 'Clase 1']
    x = np.arange(2) 
    width = 0.20  

    fig, ax = plt.subplots(figsize=(8,6))
    rects1 = ax.bar(x - width, np.round(recall_decisiontree,2), width, label='Tree')
    rects2 = ax.bar(x, np.round(recall_randomforest,2), width, label='Forest')
    rects3 = ax.bar(x + width, np.round(recall_gradientboost,2), width, label='Gradient')
    rects4 = ax.bar(x + width*2, np.round(recall_xgbost,2), width, label='XGB')

    ax.set_ylabel('Recall')
    ax.set_title('Recall En Modelos')
    ax.set_xticks(x)
    ax.set_xticklabels(labels)
    ax.legend(title="Modelos",loc="center left",bbox_to_anchor=(1.2, 0, 0.5, 1))
    
    def autolabel(rects):
        for rect in rects:
            height = rect.get_height()
            ax.annotate('{}'.format(height),
                        xy=(rect.get_x() + rect.get_width()/2, height),
                        xytext=(0, 3),
                        textcoords="offset points",
                        ha='center', va='bottom')


    autolabel(rects1)
    autolabel(rects2)
    autolabel(rects3)
    autolabel(rects4)

    fig.tight_layout()

    plt.show()


# Grafica un heatmap de las correlaciones
# Entrada: dataframe
def heatmap_corr(dataframe, annot=True):
    corr = dataframe.corr()
    mask = np.zeros_like(corr)
    mask[np.triu_indices_from(mask)] = True
    with sns.axes_style("white"):
        ax = sns.heatmap(corr, mask=mask, 
                        cmap='seismic', 
                        annot=annot, 
                        center=0, 
                        square=True,
                        xticklabels=True, yticklabels=True);
        bottom, top = ax.get_ylim();
        ax.set_ylim(bottom + 0.5, top - 0.5);
